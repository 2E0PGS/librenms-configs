# Alert rules

## Device down

```sql
SELECT * FROM devices WHERE (devices.device_id = ?) AND (devices.status = 0 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```

## Device rebooted

```sql
SELECT * FROM devices WHERE (devices.device_id = ?) AND devices.uptime < 14400 AND (devices.status = 1 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```

## Service disruption

```sql
SELECT * FROM devices,services WHERE (devices.device_id = ? AND devices.device_id = services.device_id) AND services.service_status != 0 AND (devices.status = 1 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```

## Disk usage over 80%

```sql
SELECT * FROM devices,storage WHERE (devices.device_id = ? AND devices.device_id = storage.device_id) AND storage.storage_perc >= 80 AND (devices.status = 1 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```

## Memory usage over 80%

```sql
SELECT * FROM devices,mempools WHERE (devices.device_id = ? AND devices.device_id = mempools.device_id) AND mempools.mempool_perc >= 80 AND (devices.status = 1 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```

## Device uptime over 100 days

```sql
SELECT * FROM devices WHERE (devices.device_id = ?) AND devices.uptime >= 8640000 AND (devices.status = 1 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```

## Processor usage over 85% for 10mins

```
SELECT * FROM devices,alert_device_map,processors WHERE (devices.device_id = ? AND devices.device_id = alert_device_map.device_id AND devices.device_id = processors.device_id) AND processors.processor_usage > 85 AND (devices.status = 1 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```

## Ethernet port usage over 50% for 10mins

```
SELECT * FROM devices,ports WHERE (devices.device_id = ? AND devices.device_id = ports.device_id) AND (((ports.ifOutOctets_rate*8) / ports.ifSpeed)*100) >= 10 AND (ports.ifOperStatus = "up" && ports.ifAdminStatus = "up" && (ports.deleted = 0 && ports.ignore = 0 && ports.disabled = 0)) = 1 AND (devices.status = 1 && (devices.disabled = 0 && devices.ignore = 0)) = 1
```
