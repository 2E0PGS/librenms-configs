# Default alerting template

> For use with: `alert-rules.md`

```php
System name: %sysName
Description: %description
Severity: %severity
{if %state == 0}Time elapsed: %elapsed{/if}
Timestamp: %timestamp
{if %faults}{foreach %faults}
{if %value.service_desc}
Service: %value.service_desc
Message: %value.service_message
{/if}
{/foreach}
{/if}
```
